package app.nikolaenko.andrew.myapplication

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener{

    private var isNeedToUpdate: Boolean = false

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.d("bett", "onConnectionFailed:$p0")
    }

    private var mGoogleApiClient: GoogleApiClient? = null

    override fun onResume() {
        super.onResume()

        Log.e("on Resume", "sgag")

        isNeedToUpdate = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        val menu = nav_view.menu

        val loginBtn = menu.findItem(R.id.nav_login_activity)
        val logoutBtn = menu.findItem(R.id.nav_log_out)
        val secondBtn = menu.findItem(R.id.nav_second_activity)
        val thirdBtn = menu.findItem(R.id.nav_third_activity)

        val loginStatus = Prefs.getBoolean("google_sign_in", false)
        logoutBtn.isVisible = loginStatus
        secondBtn.isVisible = loginStatus
        thirdBtn.isVisible = loginStatus

        if (loginStatus){
            loginBtn.isVisible = false
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_first_activity -> {
            }

            R.id.nav_login_activity -> {
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            }
            R.id.nav_second_activity -> {

            }
            R.id.nav_third_activity -> {

            }
            R.id.nav_log_out -> {

                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        object : ResultCallback<Status> {
                            override fun onResult(status: Status) {

                                Prefs.putBoolean("google_sign_in", false)

                                val intent = intent
                                finish()
                                startActivity(intent)
                            }
                        })
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
